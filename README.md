# binaryFileToQrCode
Convert binary files to QR codes

# Preinstall
sudo apt install -y libzbar-dev libopencv-dev qt5-default

# Build
cmake -Bbuild -DCMAKE_PREFIX_PATH=/usr/local/opt/qt5/lib/cmake -DBUILD_WITH_OpenCV=ON -DBUILD_WITH_ZBar=ON && cmake --build build
